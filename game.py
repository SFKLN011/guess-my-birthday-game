#Input username
user_name = input("What is your name?")

#Iterate over the information 4 times
for guess_number in range(5):

    #Pull the randint function
    from random import (randint)
    #Set two variables for the years specified and the 12 months
    guess_year = randint(1924,2024)
    guess_month = randint(1,12)

    #Input response from user
    response = input('yes or no')

    #Print the number of guesses, username, guessed month and guesse year.
    print(f"Guess", guess_number, ":", user_name, "were you born in", guess_month , "/", guess_year)

    #Iterate over each response to check the logic to get the printed output
    if response == "yes":
        print("I knew it!")
        break
    elif guess_number == 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
